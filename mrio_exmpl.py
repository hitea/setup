""" Small example functions for the HiTEA - dask development
"""

import time
import pymrio
import numpy as np

 
def mock_mrio_aly(wiggle=None, delay=0):
    """ Load the test mrio, calculate and get result back
     
    Parameters
    ---------
    wiggle: float
        Some random factor to perubate the A matrix (simulation MC)
    dalay: int
        Delay in sec for the calculations
        
    """
    tt = pymrio.load_test()
    if wiggle:
        tt.Z = -wiggle + (tt.Z * (wiggle-(-wiggle)))
    tt.calc_all()
    time.sleep(delay)
    result = tt.emissions.D_cba.loc[('emission_type1', 'air'), ('reg1', 'food')]
    return result

x = mock_mrio_aly(wiggle=10, delay=0)
















